# Solution

	1. BigInteger is used to handle very large inputs in Java.
	2. This problem is an epitome of fibonacci series.
	3. This problem can be solved in O(1) space, i.e., without
		 using the extra array to store intermediate results.
