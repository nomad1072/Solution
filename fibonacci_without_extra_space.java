/* package whatever; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;
import java.math.BigInteger;

/* Name of the class has to be "Main" only if the class is public. */
class Ideone
{
	public static void main (String[] args) throws java.lang.Exception
	{
		// your code goes here
		/*

		1. BigInteger is used to handle very large inputs in Java.
		2. This problem is an epitome of fibonacci series.
		3. This problem can be solved in O(1) space, i.e., without
		using the extra array to store intermediate results.

		*/
		Scanner s = new Scanner(System.in);
		BigInteger n, a, b, c;
		BigInteger m = BigInteger.valueOf(10^ 9 + 7);
		n = s.nextBigInteger();
	//	BigInteger[] a = new BigInteger[n.intValue()+1];
		a = BigInteger.valueOf(2);
		b = BigInteger.valueOf(3);
		for(int i = 2; i <= n.intValue(); i++) {
			c = a.add(b);
			a = b;
			b = c;
		}
		System.out.println(a.mod(m));

	}
}
